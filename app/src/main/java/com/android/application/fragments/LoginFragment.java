package com.android.application.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.application.R;
import com.android.application.databinding.FragmentLoginBinding;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginFragment extends Fragment {
    private FirebaseAuth auth;
    private FirebaseUser firebaseUser;
    private DatabaseReference reference;

    private FragmentLoginBinding binding;
    private OnAuthenticationComplete listener;

    public interface OnAuthenticationComplete {
        void onComplete();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAuthenticationComplete) {
            listener = (OnAuthenticationComplete) context;
        } else {
            throw new ClassCastException(context.toString() + "must implement OnAuthenticationComplete");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        binding.setLoginState(true);
        binding.progressBar.setVisibility(View.INVISIBLE);
        View view = binding.getRoot();
        ButterKnife.bind(this, view);

        auth = FirebaseAuth.getInstance();
        return view;
    }

    @OnClick(R.id.loginButton)
    void onLoginClick(View view) {
        if (validateLoginForm()) {
            String email = getEmail();
            String password = getPassword();
            signIn(email, password);
        }
    }

    @OnClick(R.id.registerButton)
    void onRegisterClick(View view) {
        if (validateRegisterForm()) {
            String email = getEmail();
            String password = getPassword();
            createAccount(email, password);
        }
    }

    @OnClick(R.id.textViewBack)
    void onBackClick(View view) {
        binding.setLoginState(true);
    }

    @OnClick(R.id.textViewOr)
    void onOrRegisterTextClick(View view) {
        binding.setLoginState(false);
    }

    private void createAccount(String email, String password) {
        binding.progressBar.setVisibility(View.VISIBLE);
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(getActivity(), task -> {
                    binding.progressBar.setVisibility(View.INVISIBLE);
                    if (task.isSuccessful()) {
                        firebaseUser = auth.getCurrentUser();
                        saveUserToDB();
                        clearFields();
                        listener.onComplete();
                        binding.setLoginState(true);
                    } else {
                        if (task.getException() != null) {
                            Toast.makeText(getActivity(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void saveUserToDB() {
        String userId = firebaseUser.getUid();
        reference = FirebaseDatabase.getInstance().getReference("Users").child(userId);

        ArrayMap<String, String> map = new ArrayMap<>(3);
        map.put("id", userId);
        map.put("username", getName());
        map.put("email", getEmail());

        reference.setValue(map).addOnCompleteListener(task -> {
            if (task.isSuccessful())
                Toast.makeText(getActivity(), "User saved into DB", Toast.LENGTH_SHORT).show();
        });
    }

    private void signIn(String email, String password) {
        binding.progressBar.setVisibility(View.VISIBLE);
        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(getActivity(), task -> {
                    binding.progressBar.setVisibility(View.INVISIBLE);
                    if (task.isSuccessful()) {
                        firebaseUser = auth.getCurrentUser();
                        clearFields();
                        listener.onComplete();
                    } else {
                        if (task.getException() != null) {
                            Toast.makeText(getActivity(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private boolean validateLoginForm() {
        return (checkInputData(binding.emailTextInput, getEmail()) &&
                checkInputData(binding.passwordTextInput, getPassword()));
    }

    private boolean validateRegisterForm() {
        return (checkInputData(binding.emailTextInput, getEmail()) &&
                checkInputData(binding.passwordTextInput, getPassword()) &&
                checkInputData(binding.nameTextInput, getName()));
    }

    private boolean checkInputData(EditText view, String value) {
        boolean valid = true;

        if (view.getId() == R.id.emailTextInput) {
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            if(!value.matches(emailPattern)) {
                view.setError("Invalid email address");
                valid = false;
            } else {
                view.setError(null);
            }
        } else {
            if (view.getId() == R.id.passwordTextInput) {
                if (value.length() < 6) {
                    view.setError("Password should be at least 6 characters");
                }
            }

            if (TextUtils.isEmpty(value)) {
                view.setError("Required!");
                valid = false;
            } else {
                view.setError(null);
            }
        }

        return valid;
    }

    private void clearFields() {
        binding.passwordTextInput.setText("");
        binding.emailTextInput.setText("");
        binding.nameTextInput.setText("");
    }

    private String getPassword() {
        return binding.passwordTextInput.getText().toString().trim();
    }

    private String getEmail() {
        return binding.emailTextInput.getText().toString().trim();
    }

    private String getName() {
        return binding.nameTextInput.getText().toString().trim();
    }

}
