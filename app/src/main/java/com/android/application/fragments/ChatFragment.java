package com.android.application.fragments;

import android.os.Bundle;
import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.application.R;
import com.android.application.adapters.ChatAdapter;
import com.android.application.vo.Message;
import com.android.application.vo.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatFragment extends Fragment {
    public static final String SELECTED_USER = "SELECTED_USER";

    @BindView(R.id.chatRecycler)
    RecyclerView chatRecycler;
    @BindView(R.id.send_btn)
    Button sendButton;
    @BindView(R.id.send_text)
    EditText sendEdittext;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private User selectedUser;
    private FirebaseUser firebaseUser;
    private FirebaseDatabase database;
    private ChatAdapter chatAdapter;
    private List<Message> messageList = new ArrayList<>();

    public static ChatFragment newInstance(User user) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(SELECTED_USER, user);
        ChatFragment fragment = new ChatFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            selectedUser = getArguments().getParcelable(SELECTED_USER);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setTitle(selectedUser.getUsername());
        toolbar.setNavigationOnClickListener(v -> {
             activity.getSupportFragmentManager()
                    .beginTransaction()
                    .remove(this)
                    .commit();
        });

        chatRecycler.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setStackFromEnd(true);
        chatRecycler.setLayoutManager(layoutManager);
        sendButton.setOnClickListener(v -> {
            String message = sendEdittext.getText().toString();
            if (message.equals("")) {
                Toast.makeText(getActivity(), "Please input text", Toast.LENGTH_SHORT).show();
            } else {
                sendMessage(firebaseUser.getUid(), selectedUser.getId(), message);
                sendEdittext.setText("");
            }
        });
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        database = FirebaseDatabase.getInstance();
        getMessages(firebaseUser.getUid(), selectedUser.getId());

        return view;
    }

    private void sendMessage(String sender, String receiver, String message) {
        ArrayMap<String, String> map = new ArrayMap<>(3);
        map.put("sender", sender);
        map.put("receiver", receiver);
        map.put("message", message);


        database.getReference().child("Chats").push().setValue(map);
    }

    private void getMessages(String currentUserId, String selectedUserId) {
        database.getReference("Chats")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        messageList.clear();
                        for (DataSnapshot data : dataSnapshot.getChildren()) {
                            Message message = data.getValue(Message.class);
                            if (message.getSender().equals(currentUserId) && message.getReceiver().equals(selectedUserId) ||
                                    message.getReceiver().equals(currentUserId) && message.getSender().equals(selectedUserId)) {
                                messageList.add(message);
                            }
                        }
                        chatAdapter = new ChatAdapter(getActivity(), messageList);
                        chatRecycler.setAdapter(chatAdapter);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
    }
}
