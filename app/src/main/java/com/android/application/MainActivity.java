package com.android.application;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.android.application.fragments.ChatFragment;
import com.android.application.fragments.LoginFragment;
import com.android.application.fragments.UsersFragment;
import com.android.application.vo.User;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity
        implements LoginFragment.OnAuthenticationComplete, UsersFragment.OnUserClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseApp.initializeApp(this);
    }

    private void changeFragment(Fragment fragment, String tag) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, fragment, tag)
                //.addToBackStack(null)  TODO
                .commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser == null ) {
            changeFragment(new LoginFragment(), "Login");
        } else {
            changeFragment(new UsersFragment(), "User");
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        FirebaseAuth.getInstance().signOut();
    }

    @Override
    public void onComplete() {
        changeFragment(new UsersFragment(), "User");
    }

    @Override
    public void onClick(User user) {
        ChatFragment fragment = ChatFragment.newInstance(user);
        changeFragment(fragment, "Message");
    }
}
