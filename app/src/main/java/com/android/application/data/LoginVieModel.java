package com.android.application.data;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class LoginVieModel extends BaseObservable {
    private boolean isLoginState;

    public LoginVieModel(boolean isLoginState) {
        this.isLoginState = isLoginState;
    }

    @Bindable
    public boolean isLoginState() {
        return isLoginState;
    }

    public void setLoginState(boolean loginState) {
        isLoginState = loginState;
        notifyChange();
    }
}
